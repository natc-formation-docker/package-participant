# Procédure d'installation

## Installation des prérequis

`apt-get update`  
`apt-get install --no-install-recommends nano git curl apache2 php5 libapache2-mod-php5 php5-gd php5-apcu php5-mcrypt php5-sqlite php5-cli php5-pgsql -y`  
`curl -sSk https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer`  
`a2enmod rewrite`  

## Récupération de l'application

`cd /home`  
`git config --global http.sslVerify false && git clone https://github.com/cachethq/Cachet.git`  
`cd Cachet`  
`git checkout v2.2.2`  
`sed -i 's/use Illuminate\\Support\\Facades\\Str;/use Illuminate\\Support\\Str;/g' app/Http/Controllers/Dashboard/SettingsController.php`  

## Configuration de l'application

`composer install --no-dev -o`  
`nano .env`  

*Fichier de paramètre :*  

```shell
APP_ENV=production  
APP_DEBUG=false  
APP_URL=http://status.cachet.com  
APP_KEY=  

DB_DRIVER=sqlite  

CACHE_DRIVER=apc  
SESSION_DRIVER=apc  
QUEUE_DRIVER=sync  
CACHET_EMOJI=false  

MAIL_DRIVER=smtp  
MAIL_HOST=mailtrap.io  
MAIL_PORT=2525  
MAIL_USERNAME=null  
MAIL_PASSWORD=null  
MAIL_ADDRESS=null  
MAIL_NAME="Demo Status Page"  
MAIL_ENCRYPTION=tls  

REDIS_HOST=null  
REDIS_DATABASE=null  
REDIS_PORT=null  

GITHUB_TOKEN=null  
```

## Création de l'application

`php artisan key:generate`  
`php artisan config:clear`  
`touch ./database/database.sqlite`  
`chmod -R 777 .env storage database bootstrap/cache`  
`php artisan app:install`  

## Configuration d'Apache

`mv /var/www/html /var/www/html-old`  
`ln -s /home/Cachet/public /var/www/html`  
`nano /etc/apache2/sites-available/000-default.conf`  

*Paramètre à ajouter après `DocumentRoot` :* 

```xml
<Directory /var/www/html>  
	Options Indexes FollowSymLinks  
	AllowOverride All  
	Require all granted  
</Directory>  
```

`service apache2 restart`  