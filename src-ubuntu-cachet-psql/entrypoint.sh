#!/bin/bash
#set -o errexit -o nounset -o pipefail

check_database_connection() {
  echo "Attempting to connect to database ..."

  prog="/usr/bin/pg_isready"
  prog="${prog} -h ${DB_HOST} -p ${DB_PORT} -U ${DB_USERNAME} -d ${DB_DATABASE} -t 1"

  timeout=30
  while ! ${prog} >/dev/null 2>&1
  do
    timeout=$(( timeout - 1 ))
    if [[ "$timeout" -eq 0 ]]; then
      echo
      echo "Could not connect to database server! Aborting..."
      exit 1
    fi
    echo -n "."
    sleep 1
  done
  echo
}

checkdbinitpsql() {
    table=sessions
    export PGPASSWORD=${DB_PASSWORD}
    if [[ "$(psql -h "${DB_HOST}" -p "${DB_PORT}" -U "${DB_USERNAME}" -d "${DB_DATABASE}" -c "SELECT to_regclass('${table}');" | grep -c "${table}")" -eq 1 ]]; then
        echo "Table ${table} exists! ..."
    else
        echo "Table ${table} does not exist! ..."
        init_db
    fi
}

initialize_system() {
  echo "Initializing Cachet container ..."

  cd /home/Cachet
  php artisan key:generate

}

init_db() {
  echo "Initializing Cachet database ..."
  php artisan config:clear
  php artisan app:install
  php artisan migrate --force
}

start_system() {
  initialize_system
  check_database_connection
  checkdbinitpsql
  echo "Starting Cachet! ..."
  php artisan config:cache
  service apache2 restart
  tail -f /var/log/apache2/access.log
}

start_system

exit 0
